### Compilation in Docker
```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.gnome.org/qarmin/gnome-shell.git
cd gnome-shell

export SONAR_SCANNER_OPTS="-Xmx4096m"
    
sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y gnome-shell
apt install -y git


git clone https://github.com/GNOME/mutter.git
cd mutter

apt build-dep -y mutter pipewire gjs
apt install -y sysprof libjack-jackd2-dev libbluetooth-dev libvulkan-dev git

git clone https://gitlab.freedesktop.org/pipewire/pipewire.git
cd pipewire

meson build/
ninja -C build/
ninja -C build/ install
cd ../
rm -rf pipewire

meson build 

ninja -C build
rm meson/meson-postinstall.sh
echo '#!/bin/bash' > meson/meson-postinstall.sh; echo 'echo 1' >> meson/meson-postinstall.sh
chmod +x meson/meson-postinstall.sh
ninja -C build/ install

cd ../
rm -rf mutter


git clone https://gitlab.gnome.org/GNOME/gjs.git
cd gjs
meson build 
ninja -C build
ninja -C build/ install
cd ../
rm -rf gjs


meson build/

ninja -C build/

```

### QTCreator Includes
```
/usr/include/freerdp2
/usr/include/glib-2.0/
plugins/
/usr/include/gtk-3.0/
/usr/include/libsecret-1/

```
